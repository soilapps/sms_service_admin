<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo $title;?></title>
 
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
  

    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet">
	
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	<style>
	.bg-dark {
    background-color: #009966 !important;
}
	</style>
  
</head>

<body>

<!--
	<div class="container">
		<div class="row">
		  <div class="col-lg-12 text-center">
			<h1 class="mt-5">Nogor Admin</h1>
		  </div>
		</div>
	</div>
-->

<!-- Navigation -->
<nav class="navbar navbar-expand-xl navbar-dark bg-dark">
      <a class="navbar-brand" href="#">SMSService</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample06" aria-controls="navbarsExample06" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExample06">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo base_url();?>admin/dashboard">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url();?>admin/manageUsers">Users</a>
          </li>
		 
		  
		
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown06" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Leads</a>
            <div class="dropdown-menu" aria-labelledby="dropdown06">
              <a class="dropdown-item" href="<?php echo base_url();?>admin/importleads">Import Leads</a>
              <!--
			  <a class="dropdown-item" href="<?php echo base_url();?>admin/exportleads">Export Leads </a>
              <a class="dropdown-item" href="<?php echo base_url();?>admin/manageleads">Manage Leads</a>
			  -->
            </div>
          </li>
		  
		   <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown06" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Settings</a>
            <div class="dropdown-menu" aria-labelledby="dropdown06">
              <a class="dropdown-item" href="<?php echo base_url();?>admin/smslimit">SMS Limit</a>
              <a class="dropdown-item" href="<?php echo base_url();?>admin/setsms">Set SMS</a>
            </div>
          </li>
		  
        </ul>
		<!--
        <form class="form-inline my-2 my-md-0">
          <input class="form-control" type="text" placeholder="Search">
        </form>
		-->
		<a class="pull-right text-right" href="<?php echo base_url();?>admin/signout" style="color: #fff">Logout</a>
      </div>
    </nav>

  <!-- Page Content -->
 <?php echo $content; ?>

	

  <!-- Bootstrap core JavaScript -->
<!--  <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.slim.min.js"></script>-->
  <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  
   	<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

</body>

</html>
