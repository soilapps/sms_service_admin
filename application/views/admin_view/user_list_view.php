 <div class="container">
    <div class="row">
      <div class="col-lg-12">
        
		<h2 class="mt-5">Users </h2>
        <p class="lead"> User List </p>
        
		<div>
		
			<table id="example" class="table table-striped table-hover  table-sm table-responsive ">
			  <thead>
				<tr>
					<th> # </th>
					<th> Full Name</th>
					<th> Email</th>		
					<th> Joining</th>
					<th> Action </th>
				</tr>
			  </thead>
			  
			  <tbody>
			  	
				<?php 
				//pd($passenger_list);
				if($user_list){
					foreach($user_list as $list){
				?>
				<tr>
				  <th scope="row"><?php echo $list->id;?></th>
				  <td><?php echo $list->name;?></td>
				  <td><?php echo $list->email;?></td>
				  <td><?php echo $list->created_at;?></td>
				 
				  <td>
					<a class="btn btn-sm btn-warning" href="<?php echo base_url(); ?>admin/edituser/<?php echo $list->id;?>"> Edit </a>   
					
					<a class="btn btn-sm btn-danger" href="<?php echo base_url(); ?>admin/deleteuser/<?php echo $list->id;?>" onclick="return confirm('Are you sure you want to delete this User?');"> Delete </a>
				  </td>
				</tr>
				<?php }
				}
				?>
			
			  </tbody>
			</table>
		</div>
    </div>
  </div>
</div>
  
  
     
<script>

$(document).ready(function() {
    $('#example').DataTable();
});

</script>