 <div class="container">
    <div class="row">
      <div class="col-lg-12">
		<h2 class="mt-5"> Set SMS </h2>
        <p class="lead"> Set SMS Text for sending  </p>
        
		<p>
		<?php echo $this->session->flashdata('msg');?>
		</p>
		
		
		<div>
		
			<form action="<?php echo base_url();?>admin/setsms" method="POST">
				<input name="id" type="hidden" value="<?php echo $message[0]->id;?>">
				  
				  <div class="form-group">
					<label for="perhitsendlimit">Message Text</label>
					<textarea name="sms_text" class="form-control"><?php echo $message[0]->message;?></textarea>
					<small id="" class="form-text text-muted">Send SMS Text here</small>
				  </div>
				  
				   <div class="form-group">
					<input type="submit" class="btn btn-primary">
				  </div>	
			</form>
			
		</div>
    </div>
  </div>
</div>
  
  
     
<script>

$(document).ready(function() {
    
});

</script>