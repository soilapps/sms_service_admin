 <div class="container">
    <div class="row">
      <div class="col-lg-12">
		<h2 class="mt-5"> Settings </h2>
        <p class="lead"> Set Sms Sending Time Limit and etc </p>
        
		<p>
		<?php echo $this->session->flashdata('msg');?>
		</p>
		
		
		<div>
		
			<form action="<?php echo base_url();?>admin/smslimit" method="POST">
				<input name="id" type="hidden" value="<?php echo $settings[0]->id;?>">
				  <div class="form-group">
					<label for="perhitsendlimit">Per Hit Send Limit</label>
					<input name="sendlimit" type="number" class="form-control" id="perhitsendlimit" aria-describedby="emailHelp" value="<?php echo $settings[0]->send_limit;?>">
					<small id="" class="form-text text-muted">Send SMS in per Load Execution</small>
				  </div>
				  <div class="form-group">
					<label for="perhitsendlimit">Load Interval in Sec</label>
					<input name="time_interval" type="number" class="form-control" id="loadinterval" value="<?php echo $settings[0]->time_interval;?>">
					<small id="" class="form-text text-muted">Send SMS Start Interval</small>
				  </div>
				  
				   <div class="form-group">
					<input type="submit" class="btn btn-primary">
				  </div>	
			</form>
			
		</div>
    </div>
  </div>
</div>
  
  
     
<script>

$(document).ready(function() {
    
});

</script>