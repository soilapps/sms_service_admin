<div  class="d-flex justify-content-center" style="display:none">
  <div id="spinner" class="spinner-border" role="status" style="position: absolute;
    top: 50%;
    z-index: 9999; display:none">
    <span class="sr-only">Loading...</span>
  </div>
</div>
 
 <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1 class="mt-5"><?php echo $title;?></h1>
        <p class="lead">Import Leads by Excell Sheet</p>
        <form method="post" id="import_form" enctype="multipart/form-data">
			<p><label>Select Excel File</label>
			<input type="file" name="file" id="file" required accept=".xls, .xlsx" /></p>
			<br />
			<input type="submit" name="import" value="Import" class="btn btn-info" />
		</form>
      </div>
    </div>
  </div>
  
  
  
<script>
$(document).ready(function(){

	/* load_data();

	function load_data()
	{
		$.ajax({
			url:"<?php echo base_url(); ?>excel_import/fetch",
			method:"POST",
			success:function(data){
				$('#customer_data').html(data);
			}
		})
	} */

	$('#import_form').on('submit', function(event){
		event.preventDefault();
		$.ajax({
			url:"<?php echo base_url(); ?>admin/import",
			method:"POST",
			data:new FormData(this),
			contentType:false,
			cache:false,
			processData:false,
			beforeSend: function( xhr ) {
				$("#spinner").show();
			},
			complete: function( xhr ) {
				$("#spinner").hide();
			},
			success:function(data){
				alert("Uploaded Successfully");
				location.reload();
			}
		})
	});

});
</script>
