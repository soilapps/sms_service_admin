<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	
	
	function __construct(){
		parent::__construct();
		$this->load->model('Model_common');
	}


	public function index()
	{
		echo "<center><h1> No Direct Access Allowed </h1></center>";
	}
	
	public function total_sms_sent(){
		
		$query = $this->db->query("SELECT `id` FROM `sms_leads` WHERE `status`='1'")->num_rows();
		echo json_encode(array("count"=>$query));
	}
	
	
	public function sms_sent(){
		
		$total_sent = $this->db->query("SELECT `id` FROM `sms_leads` WHERE `status`='1'")->num_rows();
		
		
		$today =  date("Y-m-d");
		
		$today_sent = $this->db->query("SELECT `id` FROM `sms_leads` WHERE `status`='1' AND `sent_date`='$today'")->num_rows();
				
		echo json_encode(array('total_count'=>$total_sent,'today_sent'=>$today_sent));
	}
	
		
	public function lead_for_reply(){
		
		//$send_limit = $this->send_limit();
		
		$lead_data = $this->db->query("SELECT `phone_number`,`id` FROM `sms_leads` WHERE `status`='0' ORDER BY `id` ASC LIMIT 2")->result();
		
		//$lead_id 	= $lead_data[0]->id;
		//$lead 		= $lead_data[0]->phone_number;
		
		$sent_date = date("Y-m-d");
		
		//$this->db->query("UPDATE `sms_leads` SET `status`='1',`sent_date`='$sent_date' WHERE `id`='$lead_id'");
		$this->db->query("UPDATE `sms_leads` SET `status`='1',`sent_date`='$sent_date' WHERE `status`='0' ORDER BY `id` ASC LIMIT 10");
		$sms_text = $this->db->query("SELECT `message` FROM `sms_message` ORDER BY `id` LIMIT 1")->result();
		
		$message = $sms_text[0]->message;
		
		
		//echo json_encode(array('lead'=>$lead,'lead_id'=>$lead_id,'message'=>$message));
		echo json_encode(array('lead'=>$lead_data,'message'=>$message));
	}
	
	
// Update sucess status//	
	public function update_status(){
		
		$lead_id = $this->input->post('lead_id');
		$sent_date = date("Y-m-d");
		
		
		$this->db->query("UPDATE `sms_leads` SET `status`='1',`sent_date`='$sent_date' WHERE `id`='$lead_id'");
		
	}
	
	
	public function error_status(){
		
		$lead_id = $this->input->post('lead_id');
		$error = $this->input->post('error');
		$sent_date = date("Y-m-d");
		
		
		$this->db->query("UPDATE `sms_leads` SET `status`='0',`report`='$error',`sent_date`='$sent_date' WHERE `id`='$lead_id'");
		
	}
	
	
// Settings //


	private function send_limit(){
		
		$query = $this->db->query("SELECT `send_limit` FROM `settings`")->result();
		
		return $send_limit = $query[0]->send_limit;
		
	}

	
	public function get_settings(){
		
		$query = $this->db->query("SELECT * FROM `settings`")->result();
		
		$time_interval = $query[0]->time_interval;
		$send_limit = $query[0]->send_limit;
		
		echo json_encode(array('time_interval'=>$time_interval,'send_limit'=>$send_limit));
	}
//
	

	
}
