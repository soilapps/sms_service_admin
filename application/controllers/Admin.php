<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	
	function __construct(){
		parent::__construct();
		
		$this->check_isvalidated();
		
		$this->load->model('Model_admin');
		$this->load->model('Model_common');
		
		$this->load->model('excel_import_model');
		$this->load->library('excel');
		//$this->load->model('Model_statistics');
		
	}


	public function index()
	{
		echo "<center><h1> No Direct Access Allowed </h1></center>";
	}
	
	public function dashboard(){
		
		$data['title'] = "Dashboard";
		
		
		
		$data['content'] = $this->load->view("admin_view/dashboard_view", $data, true);

		$this->load->view("admin_view/common/main_view", $data);
	}
	
// SMS service //	
	public function importleads(){
		$data['title'] = "Import Leads";
		$data['content'] = $this->load->view("admin_view/importleads_view", $data, true);
		$this->load->view("admin_view/common/main_view", $data);
	}
	
	public function import()
	{
		if(isset($_FILES["file"]["name"]))
		{
			$path = $_FILES["file"]["tmp_name"];
			$object = PHPExcel_IOFactory::load($path);
			foreach($object->getWorksheetIterator() as $worksheet)
			{
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();
				for($row=2; $row<=$highestRow; $row++)
				{
					$phone_number = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					$name = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					
					$data[] = array(
						'user_id'		=>	0,
						'campaign_id'	=>	0,
						'phone_number'	=>	$phone_number,
						'name'		=>	'-',
						'age'		=>	0,
						'status'	=>	0
					);
				}
			}
			$this->excel_import_model->insert($data);
			echo json_encode(array("message"=>"Data Imported successfully"));
		}	
	}
	
// SMS LIMIT //
	public function smslimit(){
		
		$data['title'] = "Set limit";
		
		if($this->input->post()){
		
			$id 			= $this->input->post("id");
			$time_interval 	= $this->input->post("time_interval");
			$sendlimit 		= $this->input->post("sendlimit");
			
			$this->db->query("UPDATE `settings` SET `time_interval`='$time_interval',`send_limit`='$sendlimit',`status`='1' WHERE `id`='$id'");
			
			$redirect = "admin/smslimit";
			
			sfr("Updated Successfully", $redirect);
		
		}else{
			$data['settings'] = $this->Model_common->databysql("SELECT `settings`.* FROM `settings`");
		
			$data['content'] = $this->load->view("admin_view/settings_view", $data, true);
		
			$this->load->view("admin_view/common/main_view", $data);
		}
	}


	
// SMS SET //
	public function setsms(){
		
		$data['title'] = "Set SMS";
		
		if($this->input->post()){
		
			$id 			= $this->input->post("id");
			$sms_text 	= $this->input->post("sms_text");
						
			$this->db->query("UPDATE `sms_message` SET `message`='$sms_text' WHERE `id`='$id'");
			
			$redirect = "admin/setsms";
			
			sfr("Updated Successfully", $redirect);
		
		}else{
			$data['message'] = $this->Model_common->databysql("SELECT `sms_message`.* FROM `sms_message`");
		
			$data['content'] = $this->load->view("admin_view/setsms_view", $data, true);
		
			$this->load->view("admin_view/common/main_view", $data);
		}
	}	
// Manage Users	
	public function manageUsers(){
		
		$data['title'] = "Users Manage";
		$data['user_list'] = $this->Model_common->databysql("SELECT `users`.* FROM `users`");
		
		$data['content'] = $this->load->view("admin_view/user_list_view", $data, true);
		
		$this->load->view("admin_view/common/main_view", $data);
	}
	
	

	
	private function check_isvalidated(){
		if($this->session->userdata('logged_in')!=true){
			$this->session->set_flashdata('msg','<div class="alert alert-warning" role="alert">
												<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
												<strong>Thanks !</strong> You are Logged Out 
												</div>');
			$url = base_url();
		    redirect($url);
		}
	}
	
	public function signout(){
		$this->session->sess_destroy(); 
		$destination = base_url()."admin_login";
		redirect($destination);
	}
	
}
